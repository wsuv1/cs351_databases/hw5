import json
import pandas

# object that contains all meta data about a Tuple
class Tuple:
    budget = ""
    genres = ""
    homepage = ""
    ident = ""
    keywords = ""
    original_language = ""
    original_title = ""
    overview = ""
    popularity = ""
    production_companies = ""
    production_countries = ""
    release_date = ""
    revenue = ""
    runtime = ""
    spoken_languages = ""
    status = ""
    tagline = ""
    title = ""
    vote_average = ""
    vote_count = ""

    def __init__(self, index, budget, genres, homepage, ident, keywords,
    original_language, original_title, overview, popularity,
    production_companies, production_countries, release_date, revenue, runtime,
    spoken_languages, status, tagline, title, vote_average, vote_count):
        self.index = index
        self.budget = budget
        self.genres = genres
        self.homepage = homepage
        self.ident = ident
        self.keywords = keywords
        self.original_language = original_language
        self.original_title = original_title
        self.overview = overview
        self.popularity = popularity
        self.production_companies = production_companies
        self.production_countries = production_countries
        self.release_date = release_date
        self.revenue = revenue
        self.runtime = runtime
        self.spoken_languages = spoken_languages
        self.status = status
        self.tagline = tagline
        self.title = title
        self.vote_average = vote_average
        self.vote_count = vote_count

def parseIntoRelations(database, inputFile):
    # intialize array to hold all tuples
    array = []
    # create a cursor object to execute MySQL
    cursorObject = database.cursor()
    # unpack csv file using pandas library
    data = pandas.read_csv(inputFile, index_col=False)
    # create a data frame for unpacked data
    df = pandas.DataFrame(data)

    # for loop goes from 0 to last row of data frame
    for x in range(df.index.stop):
        # checks for nan, since runtime gets mad when you put a string
        # in it
        if (df.loc[x].runtime != df.loc[x].runtime):
            runtime = "0"
        else:
            runtime = df.loc[x].runtime
        
        # creates a tuple based on this row of the dataframe and stores it into
        # the array
        array.append(
            Tuple(
                df.loc[x].index, df.loc[x].budget, df.loc[x].genres,
                df.loc[x].homepage, df.loc[x].id, df.loc[x].keywords,
                df.loc[x].original_language, df.loc[x].original_title,
                df.loc[x].overview, df.loc[x].popularity,
                df.loc[x].production_companies, df.loc[x].production_countries,
                df.loc[x].release_date, df.loc[x].revenue, runtime,
                df.loc[x].spoken_languages, df.loc[x].status, df.loc[x].tagline,
                df.loc[x].title, df.loc[x].vote_average, df.loc[x].vote_count
            )
        )

        # creates a MySQL query template
        sql = """
            INSERT INTO MOVIE (BUDGET, HOMEPAGE, ID, ORIG_LANG, ORIG_TITLE,
            OVERVIEW, POPULARITY, REL_DATE, REVENUE, RUNTIME, STATUS, TAGLINE,
            TITLE, VOTE_AVG, VOTE_COUNT) VALUES (%s, %s, %s, %s, %s, %s, %s,
            %s, %s, %s, %s, %s, %s, %s, %s)
        """
        # values to load into the MySQL query
        val = (
            str(df.loc[x].budget), str(df.loc[x].homepage), str(df.loc[x].id),
            str(df.loc[x].original_language), str(df.loc[x].original_title),
            str(df.loc[x].overview), str(df.loc[x].popularity),
            str(df.loc[x].release_date), str(df.loc[x].revenue), str(runtime),
            str(df.loc[x].status), str(df.loc[x].tagline), str(df.loc[x].title),
            str(df.loc[x].vote_average), str(df.loc[x].vote_count)
        )
        # execute the MySQL query with the values loaded in
        cursorObject.execute(sql, val)
    print("MOVIE table has been populated.")

    # 'GENRE' is the start state
    mode = 'GENRE'
    # 'END' is the end state, we loop until we reach it
    while (mode != "END"):
        i = 0
        # we loop through each Tuple in the array, and parse the current state's
        # json list
        for x in array:
            # here we specify which state we're in and parse the corresponding
            # JSON string using the json library
            if   (mode == "GENRE"):
                result = json.loads(array[i].genres)
            elif (mode == "KEYWORD"):
                result = json.loads(array[i].keywords)
            elif (mode == "PRODUCER"):
                result = json.loads(array[i].production_companies)
            elif (mode == "COUNTRY"):
                result = json.loads(array[i].production_countries)
            elif (mode == "LANGUAGE"):
                result = json.loads(array[i].spoken_languages)
            # now we loop through each item in the JSON string
            for j in result:
                # here we have a template sql statement
                sql = """
                    INSERT IGNORE INTO PLACEHOLDER (ID, NAME)
                    VALUES (%s, %s);
                """
                # we replace PLACEHOLDER with the name of our current state
                sql = sql.replace("PLACEHOLDER", mode)
                
                # since the ID isn't always called ID, we have special edge
                # case statements here.
                if (mode == "LANGUAGE"):
                    val = (j["iso_639_1"], j["name"])
                elif (mode == "COUNTRY"):
                    val = (j["iso_3166_1"], j["name"])
                else:
                    val = (j["id"], j["name"])
                
                # we execute the sql statement we built above
                cursorObject.execute(sql, val)

                # here we have another template sql statement that populates the
                # movie_into_* tables
                sql = """
                    INSERT IGNORE INTO MOVIE_TO_PLACEHOLDER (MOVIE_ID,
                    PLACEHOLDER_ID) VALUES (%s, %s);
                """
                # we replace PLACEHOLDER with the name of the current state
                sql = sql.replace("PLACEHOLDER", mode)

                # since the ID isn't always called ID, we have special edge
                # case statements here.
                if (mode == "LANGUAGE"):
                    val = (str(array[i].ident), j["iso_639_1"])
                elif (mode == "COUNTRY"):
                    val = (str(array[i].ident), j["iso_3166_1"])
                else:
                    val = (str(array[i].ident), j["id"])

                # we execute sql statement built above
                cursorObject.execute(sql, val)

            # next row
            i += 1

        # change state
        if   (mode == "GENRE"):
            print("GENRE table has been populated.")
            mode = "KEYWORD"
        elif (mode == "KEYWORD"):
            print("KEYWORD table has been populated.")
            mode = "PRODUCER"
        elif (mode == "PRODUCER"):
            print("PRODUCER table has been populated.")
            mode = "COUNTRY"
        elif (mode == "COUNTRY"):
            print("COUNTRY table has been populated.")
            mode = "LANGUAGE"
        elif (mode == "LANGUAGE"):
            print("LANGUAGE table has been populated.")
            mode = "END"

    # commit changes to sql server
    database.commit()