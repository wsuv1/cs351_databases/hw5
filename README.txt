                *--------------*
                | Steps to Run |
                *--------------*
- 1 : run sudo pip install -r requirements.txt
- 2 : run python main.py
- 3 : follow the command prompts
- 4 : profit

* note : If you run the program again, it will   *
*        delete the last database it created and *
*        create a new one in it's place.         *