use hw5;
# 1. 10% What is the average budget of all movies?
# Your output should include just the average budget value.
SELECT AVG(BUDGET) FROM MOVIE;

# 2. 10% Show the movies that were produced in the United States.
# Your output must include the movie title and the production company name.
SELECT TITLE, PRODUCER.NAME
FROM (
	(((MOVIE JOIN MOVIE_TO_PRODUCER ON MOVIE.ID=MOVIE_TO_PRODUCER.MOVIE_ID)
    JOIN PRODUCER ON PRODUCER.ID=MOVIE_TO_PRODUCER.PRODUCER_ID)
    JOIN MOVIE_TO_COUNTRY ON MOVIE.ID=MOVIE_TO_COUNTRY.MOVIE_ID)
    JOIN COUNTRY ON COUNTRY.ID=MOVIE_TO_COUNTRY.COUNTRY_ID
) WHERE (
	COUNTRY.ID='US'
);

# 3. 10% Show the top 5 movies that made the most revenue.
# Your output must include the movie title and how much revenue it brought in.
SELECT TITLE, REVENUE FROM MOVIE ORDER BY REVENUE DESC LIMIT 5;

# 4. 10% What movies have both the genre Science Fiction and Mystery.
#Your output must include the movie title and all genres associated with that movie.
SELECT MOVIE.TITLE, GENRE.NAME
FROM (
	(MOVIE JOIN MOVIE_TO_GENRE ON MOVIE.ID=MOVIE_TO_GENRE.MOVIE_ID)
    JOIN GENRE ON MOVIE_TO_GENRE.GENRE_ID=GENRE.ID
) WHERE (
	MOVIE.ID=ANY (
		SELECT MOVIE.ID FROM (
			(MOVIE JOIN MOVIE_TO_GENRE ON MOVIE.ID=MOVIE_TO_GENRE.MOVIE_ID)
			JOIN GENRE ON MOVIE_TO_GENRE.GENRE_ID=GENRE.ID
		) WHERE (
			GENRE.NAME='Science Fiction'
		)
	) AND MOVIE.ID=ANY (
		SELECT MOVIE.ID FROM (
			(MOVIE JOIN MOVIE_TO_GENRE ON MOVIE.ID=MOVIE_TO_GENRE.MOVIE_ID)
			JOIN GENRE ON MOVIE_TO_GENRE.GENRE_ID=GENRE.ID
		) WHERE (
			GENRE.NAME='Mystery'
		)
	)
);

# 5. 10% Find the movies that have a popularity greater than the average popularity.
# Your output must include the movie title and their popularity.
SELECT TITLE, POPULARITY FROM MOVIE
WHERE POPULARITY > (SELECT AVG(POPULARITY) FROM MOVIE);