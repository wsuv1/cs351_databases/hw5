
def createRelations(database):
    cursorObject = database.cursor()

    # movie relation, this primary key (ID) is used as a link to all the join tables
    movie = """
        CREATE TABLE MOVIE (
            BUDGET INT NOT NULL,
            HOMEPAGE VARCHAR(256) NOT NULL,
            ID INT NOT NULL,
            ORIG_LANG VARCHAR(2) NOT NULL,
            ORIG_TITLE VARCHAR(128) NOT NULL,
            OVERVIEW VARCHAR(1028) NOT NULL,
            POPULARITY DOUBLE NOT NULL,
            REL_DATE VARCHAR(10) NOT NULL,
            REVENUE BIGINT NOT NULL,
            RUNTIME INT NOT NULL,
            STATUS VARCHAR(16) NOT NULL,
            TAGLINE VARCHAR(256) NOT NULL,
            TITLE VARCHAR(128) NOT NULL,
            VOTE_AVG FLOAT NOT NULL,
            VOTE_COUNT INT NOT NULL,
            PRIMARY KEY (ID)
        );
    """
    # execute sql command
    cursorObject.execute(movie)

    # GENRE is the first state
    mode = 'GENRE'
    # we loop until we reach the END state
    while (mode != "END"):
        # template sql command to create a generic table
        sql = """
            CREATE TABLE PLACEHOLDER (
                ID TYPE NOT NULL,
                NAME VARCHAR(32) NOT NULL,
                PRIMARY KEY (ID)
            );
        """
        # we replace PLACEHOLDER with our current state name
        sql = sql.replace("PLACEHOLDER", mode)

        # we replace TYPE with the type we need for the current state
        if ((mode == "LANGUAGE") | (mode == "COUNTRY")):
            sql = sql.replace("TYPE", "VARCHAR(2)")
        else:
            sql = sql.replace("TYPE", "INT")

        # execute sql built above
        cursorObject.execute(sql)

        # here we have another templace sql query for a generic relation table
        sql = """
            CREATE TABLE MOVIE_TO_PLACEHOLDER (
                MOVIE_ID INT NOT NULL,
                PLACEHOLDER_ID TYPE NOT NULL,
                FOREIGN KEY (MOVIE_ID) REFERENCES MOVIE(ID),
                FOREIGN KEY (PLACEHOLDER_ID) REFERENCES PLACEHOLDER(ID)
            );
        """

        # we replace PLACEHOLDER with name of current state
        sql = sql.replace("PLACEHOLDER", mode)

        # we replace TYPE with the type we need for the current state
        if ((mode == "LANGUAGE") | (mode == "COUNTRY")):
            sql = sql.replace("TYPE", "VARCHAR(2)")
        else:
            sql = sql.replace("TYPE", "INT")

        # execute sql built above
        cursorObject.execute(sql)

        # change state
        if   (mode == "GENRE"):
            mode = "KEYWORD"
        elif (mode == "KEYWORD"):
            mode = "PRODUCER"
        elif (mode == "PRODUCER"):
            mode = "COUNTRY"
        elif (mode == "COUNTRY"):
            mode = "LANGUAGE"
        elif (mode == "LANGUAGE"):
            mode = "END"

    print()
    print("Created tables MOVIE, GENRE, KEYWORD, PRODUCER, COUNTRY, LANGUAGE")
    print("and corresponding relation tables.")
    print()
    # commit sql we wrote in loop above
    database.commit()