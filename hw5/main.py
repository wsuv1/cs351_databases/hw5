import createRelations, parseIntoRelations, mysql.connector

def main():

    # User input to get MySQL information
    print("Welcome, this is a tool that parses the 'tmdb_5000_movies.csv' file")
    print("and populates a database from it's data! We just need some information")
    print("about your MySQL server to get started.\n")
    hostname = input("SQL Host: ")
    username = input("User: ")
    password = input("Password: ")
    print()

    # The following code deletes the hw5 database if it exists, and creates a new one
    try:
        database   = mysql.connector.connect(
            host   = hostname,
            user   = username,
            passwd = password
        )
    except:
        print("Something went wrong connecting to that server.")
        exit()

    cursorObject = database.cursor()

    # delete the database if it exists and create a new one
    cursorObject.execute("DROP DATABASE IF EXISTS hw5;")
    cursorObject.execute("CREATE DATABASE hw5;")

    database.close()

    # The following code opens a connection to the mysql server on the hw5 database
    database     = mysql.connector.connect(
        host     = hostname,
        user     = username,
        passwd   = password,
        database = "hw5"
    )

    print("A new database called 'hw5' has been successfully created.")

    # calls main functions then quits
    createRelations.createRelations(database)
    parseIntoRelations.parseIntoRelations(database, 'tmdb_5000_movies.csv')
    database.close()
    print("Goodbye!")

main()